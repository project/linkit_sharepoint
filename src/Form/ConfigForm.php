<?php

namespace Drupal\linkit_sharepoint\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\Role;

/**
 * Config form class.
 *
 * @package Drupal\linkit_sharepoint\Form
 */
class ConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'linkit_sharepoint.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'linkit_sharepoint_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('linkit_sharepoint.settings');

    $form['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL'),
      '#description' => $this->t('The Sharepoint service URL, e.g. <em>https://srv-80-24-44.dwezed.local/demo/drupal</em>'),
      '#default_value' => $config->get('url'),
    ];
    $form['list_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('List Title'),
      '#description' => $this->t('The list title, e.g. <em>Dokumente</em>'),
      '#default_value' => $config->get('list_title'),
    ];
    $form['user'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Technical User'),
      '#description' => $this->t('The username, e.g. <em>firstname.lastname</em> (no domain prefix)'),
      '#default_value' => $config->get('user'),
    ];
    $form['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Technical User Password'),
      '#description' => $this->t('The user password, leave empty if unchanged'),
      '#default_value' => $config->get('password'),
    ];
    $form['roleassignments'] = [
      '#type' => 'table',
      '#caption' => $this->t('Role Assignment. Last three rows are for new entries. To delete a row, delete all contents of the row. Sharepoint users without domain prefix.'),
      '#header' => [
        $this->t('Drupal Role'),
        $this->t('SharePoint User'),
      ],
    ];
    $roleAssignments = $config->get('roleassignments');
    if (!$roleAssignments) {
      $roleAssignments = [];
    }
    $roleChoice = ['' => t('- Please Choose -')];
    foreach (Role::loadMultiple() as $r) {
      $roleChoice[$r->get('id')] = $r->get('label');
    }

    $count = count($roleAssignments) + 3;
    for ($i = 0; $i < $count; $i++) {
      $form['roleassignments'][$i]['drole'] = [
        '#type' => 'select',
        '#title_display' => 'invisible',
        '#options' => $roleChoice,
        '#default_value' => ($i < $count - 3) ? $roleAssignments[$i]['drole'] : '',
      ];
      $form['roleassignments'][$i]['srole'] = [
        '#type' => 'textfield',
        '#title_display' => 'invisible',
        '#default_value' => ($i < $count - 3) ? $roleAssignments[$i]['srole'] : '',
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Extract only fully filled in assignments.
    $saveAssignments = [];
    foreach ($form_state->getValue('roleassignments') as $a) {
      if ($a['drole'] && $a['srole']) {
        $saveAssignments[] = $a;
      }
    }

    $config = $this->config('linkit_sharepoint.settings')
      ->set('url', $form_state->getValue('url'))
      ->set('list_title', $form_state->getValue('list_title'))
      ->set('user', $form_state->getValue('user'))
      ->set('roleassignments', $saveAssignments);

    if ($form_state->getValue('password')) {
      $config->set('password', $form_state->getValue('password'));
    }
    $config->save();
  }

}
