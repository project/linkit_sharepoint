<?php

namespace Drupal\linkit_sharepoint;

use Drupal\Core\Config\ConfigFactoryInterface;
use Office365\Runtime\Auth\NetworkCredentialContext;
use Office365\SharePoint\ClientContext;
use Office365\SharePoint\ListItem;

/**
 * Linkit service.
 */
class Sharepoint {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The client context.
   *
   * @var \Office365\SharePoint\ClientContext
   */
  public $context;

  /**
   * The web site.
   *
   * @var \Office365\SharePoint\Web
   */
  public $web;

  /**
   * A sharepoint list.
   *
   * @var \Office365\SharePoint\SPList
   */
  public $list;

  /**
   * Constructs a Sharepoint object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('linkit_sharepoint.settings');
  }

  /**
   * Init the Office365 ClientContext necessary for all SP calls.
   */
  public function initializeContext() {
    $authContext = new NetworkCredentialContext(
      $this->config->get('user'),
      $this->config->get('password')
    );

    // CURLAUTH_BASIC; Default auth schema.
    $authContext->AuthType = CURLAUTH_NTLM;

    // Initialize REST client.
    $this->context = new ClientContext($this->config->get('url'), $authContext);
    $this->web = $this->context
      ->getWeb();
    $this->list = $this->web->getLists()
      ->getByTitle($this->config->get('list_title'));
  }

  /**
   * Search for a list item.
   *
   * @param string $search_key
   *   The search key.
   *
   * @return array
   *   An array of search results.
   */
  public function search($search_key) {
    $this->initializeContext();

    // Filter $search_key for bad characters.
    $search_key = preg_replace("/[^a-zA-Z0-9äöüÄÖÜ]+/", "", $search_key);
    $items = $this->list
      ->getItems()
      ->select('*,EncodedAbsUrl')
      ->filter('substringof(\'' . $search_key . '\',TaxCatchAll/Term) or substringof(\'' . $search_key . '\',Title)');

    $this->context->load($items);
    try {
      $this->context->executeQuery();
    }
    catch (\Exception $e) {
      \Drupal::logger('linkit_sharepoint')->error($e->getMessage());
      return [];
    }

    return (array) $items->getData();
  }

  /**
   * Given a SP ListItem, get the SP/AD usernames which may read the item.
   *
   * @param \Office365\SharePoint\ListItem $listItem
   *   A sharepoint list item.
   *
   * @return array
   *   An array of usernames with read access.
   *
   * @throws \Exception
   */
  public function getReadUsers(ListItem $listItem) {
    $outArr = $assignments = $bindings = [];
    /** @var \Office365\SharePoint\RoleAssignment $assignment */
    foreach ($listItem->getRoleAssignments()->getIterator() as $assignment) {
      $assignments[] = $assignment;
      /** @var \Office365\SharePoint\RoleDefinition $binding */
      foreach ($assignment->getRoleDefinitionBindings()->getIterator() as $binding) {
        $bindings[$assignment->getPrincipalId()][] = $binding->getName();

        // Available permissions from Sharepoint are currently
        // 'Full Control', 'Limited Access', 'Lesen', 'Bearbeiten'.
        if ($binding->getName() === 'Lesen'
          || $binding->getName() === 'Full Control'
          || $binding->getName() === 'Bearbeiten') {

          $login_name = $assignment->getMember()->getLoginName();
          $startPos = strpos($login_name, '\\');
          // We strip unnecessary domain information.
          if ($startPos > 0) {
            $outArr[] = substr($login_name, $startPos + 1);
          }
          else {
            $outArr[] = $login_name;
          }
        }
      }
    }
    return $outArr;
  }

}
