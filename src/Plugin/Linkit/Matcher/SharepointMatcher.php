<?php

namespace Drupal\linkit_sharepoint\Plugin\Linkit\Matcher;

use Drupal\linkit\MatcherBase;
use Drupal\linkit\Suggestion\SimpleSuggestion;
use Drupal\linkit\Suggestion\SuggestionCollection;

/**
 * Sharepoint matcher class.
 *
 * @Matcher(
 *   id = "sharepoint_matcher",
 *   label = @Translation("Sharepoint Matcher"),
 * )
 */
class SharepointMatcher extends MatcherBase {

  /**
   * {@inheritdoc}
   */
  public function execute($string) {
    $sp = \Drupal::service('linkit_sharepoint.sharepoint');
    $items = $sp->search($string);

    $config = \Drupal::service('config.factory')->get('linkit_sharepoint.settings');
    $metadata = $config->get('metadata');

    $suggestions = new SuggestionCollection();
    foreach ($items as $item) {
      $description = [];

      foreach ($metadata as $name => $attributes) {
        $property = $item->getProperty($name);
        if (!empty($property)) {
          $description[] = $this->t('@name: @label', [
            '@name' => $this->t($name),
            '@label' => isset($attributes[$property['Label']]) ? $this->t($attributes[$property['Label']]) : $this->t($property['Label']),
          ]);
        }
      }

      $encodedAbsUrl = $item->getProperty('EncodedAbsUrl');
      $description[] = $encodedAbsUrl;

      $suggestion = new SimpleSuggestion();
      $title = $item->getProperty('Title') ?: urldecode(basename($encodedAbsUrl));
      $title .= implode('<br>', $description);
      $suggestion
        ->setLabel($title)
        ->setPath('/deliver/' . $item->getProperty('Id'))
        ->setGroup('Sharepoint Documents');

      $suggestions->addSuggestion($suggestion);
    }

    return $suggestions;
  }

}
