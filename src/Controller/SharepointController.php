<?php

namespace Drupal\linkit_sharepoint\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Core\Controller\ControllerBase;
use Office365\Runtime\Http\RequestOptions;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

/**
 * Sharepoint controller class.
 *
 * @package Drupal\linkit_sharepoint\Controller
 */
class SharepointController extends ControllerBase {

  /**
   * The module configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * The Sharepoint service.
   *
   * @var \Drupal\linkit_sharepoint\Sharepoint
   */
  protected $sharepoint;

  /**
   * SharepointController constructor.
   */
  public function __construct() {
    $this->config = \Drupal::service('config.factory')
      ->get('linkit_sharepoint.settings');
    $this->currentUser = \Drupal::service('current_user');
    $this->sharepoint = \Drupal::service('linkit_sharepoint.sharepoint');
  }

  /**
   * Menu callback for autocompletion.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response containing the autocomplete suggestions for Views tags.
   */
  public function autocompleteDocument(Request $request) {
    $matches = [];
    $search_key = $request->query->get('q');
    $items = $this->sharepoint->search($search_key);

    /** @var \Office365\SharePoint\ListItem $item */
    foreach ($items as $item) {
      $matches[] = [
        'value' => $item->getId(),
        'label' => Html::escape($item->getProperty('Title')) . ' (' . $item->getId() . ')',
      ];
    }
    return new JsonResponse($matches);
  }

  /**
   * Document delivery and access check.
   *
   * Given the numeric ID of an SP document, this checks if the user may
   * access it. If yes, it sends the document to the user without saving the
   * file locally.
   *
   * @param int $id
   *   The Sharepoint document id.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response object.
   *
   * @throws \Exception
   */
  public function deliver($id) {
    $this->sharepoint->initializeContext();
    $items = $this->sharepoint->list->getItems()->select('*,EncodedAbsUrl')
      ->expand('Roleassignments/Member,RoleAssignments/RoleDefinitionBindings')
      ->filter('Id eq ' . $id);

    // Save a query to retrieve list items from the server.
    $this->sharepoint->context->load($items);
    // Submit query to SharePoint Online REST service.
    $this->sharepoint->context->executeQuery();

    // We cannot access this object collection like an array, so we do it
    // this way.
    foreach ($items->getData() as $item) {
      break;
    }

    // ID did not exist; there was no item in the list.
    if (!isset($item)) {
      throw new UnauthorizedHttpException('');
    }

    // Check if user may read this document.
    $doDeliver = FALSE;
    // Array(drole), i. e., authenticated oder administrator.
    $droles = $this->currentUser->getRoles();
    // Array('drole', 'srole').
    $roleassignments = $this->config->get('roleassignments');
    // Array(srole).
    $sroles = $this->sharepoint->getReadUsers($item);
    foreach ($droles as $drole) {
      foreach ($roleassignments as $roleassignment) {
        if ($roleassignment['drole'] == $drole) {
          if (in_array($roleassignment['srole'], $sroles)) {
            $doDeliver = TRUE;
            break 2;
          }
        }
      }
    }
    if (!$doDeliver) {
      throw new UnauthorizedHttpException('');
    }

    $encodedAbsURL = $item->getProperty('EncodedAbsUrl');
    $options = new RequestOptions($encodedAbsURL);
    // Follow file relocations.
    $options->FollowLocation = TRUE;
    $data = $this->sharepoint->context->executeQueryDirect($options);

    // Construct binary response w/o saving the file locally.
    $response = new Response();
    $response->setContent($data->getContent());

    // We assume the data is usually binary.
    $response->headers->set('Content-Type', 'application/octet-stream');

    // Get only the filename.
    $filenameArr = explode('/', $encodedAbsURL);

    // Send the file with its original filename if present.
    $dispositionHeader = $response->headers->makeDisposition(
      ResponseHeaderBag::DISPOSITION_ATTACHMENT,
      urldecode($filenameArr[count($filenameArr) - 1]),
      'unkown.filename');
    $response->headers->set('Content-Disposition', $dispositionHeader);

    return $response;
  }

}
